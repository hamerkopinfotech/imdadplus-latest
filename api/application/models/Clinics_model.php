<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:03 AM
 */

class Clinics_model extends CI_Model
{

    /**
     * Get all country data based on filter
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getAll( $filters = array() ){

        $sql = "SELECT  cl.Clinic_Id, cl.Clinic_Name,ct.City_Name,cr.Country_Name FROM tbl_clinics as cl join tbl_city as ct on cl.City_Id = ct.City_Id join tbl_country as cr on cl.Country_Id = cr.Country_Id";
        $query = $this->db->query( $sql );

        // echo $sql;die;

        return $query->result();
    }

}