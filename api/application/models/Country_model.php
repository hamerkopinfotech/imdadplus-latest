<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 9:03 AM
 */

class Country_model extends CI_Model
{

    /**
     * Get country marker data
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getMarkerData( $data ){

        $where = array();

        $country_id = isset( $data['country_id'] ) ? (int)$data['country_id'] : 0;
        $city_id = isset( $data['city_id'] ) ? (int)$data['city_id'] : 0;

        // can be array fo devices
        $device_id = isset( $data['devices_id'] ) ? $data['devices_id'] : 0;

        if( $country_id > 0 ){
            $where[] = 'tc.Country_Id = '. $country_id;
        }

        if( $city_id ){
            $where[] = 'tc.City_Id = '. $city_id;

        }

        if( is_array( $device_id ) && count( $device_id ) > 0 ){
            $where[] = 'tdi.Device_Id IN ('. implode(',', $device_id ) .')';

        }
        elseif( $device_id > 0 ){
            $where[] = 'tdi.Device_Id = '. $device_id;

        }

        $where = implode(' AND ', $where );
        if( $where ){
            $where = "WHERE $where";
        }

        //$sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat, c.country_centre_long FROM tbl_country AS c INNER JOIN tbl_clinics AS tc ON tc.Country_Id = c.Country_Id INNER JOIN tbl_device_install AS tdi ON tdi.Clinic_Id = tc.Clinic_Id $where";
		
		$sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat, c.country_centre_long FROM tbl_country AS c $where";

        $query = $this->db->query( $sql );

        $result = $query->result();

        /*
        old query
        if( isset($filters['devices_id']) && $filters['devices_id'] > 0 ){
            $this->db->join('tbl_map_report', 'tbl_map_report.Country_Id = tbl_country.Country_Id');

            if( is_array( $filters['devices_id']  ) && count( $filters['devices_id']  ) > 0 ){
                // foreach( $filters['devices_id'] as $device ){
                //     $this->db->where('tbl_map_report.Device_Id', $device );
                // }
                $this->db->where_in('tbl_map_report.Device_Id', $filters['devices_id'] );
            }
            elseif( $filters['devices_id']  > 0 ){
                $this->db->where('tbl_map_report.Device_Id', $filters['devices_id'] );
            }

        }


        $query = $this->db->get('tbl_country');

        $result = $query->result();
        */

        $_country = array();

        if( count( $result ) ){
            foreach( $result as $country ){
                $_country[slugify( $country->Country_Name )] = array(
                    'id'            => $country->Country_Id,
                    'lat'           => $country->Country_Latitude,
                    'lng'           => $country->Country_Longitude,
                    'label'         => $country->Country_Name,
                    'slug'          => slugify( $country->Country_Name ),
                    'type'          => 'country',
                    'zoom'          => $country->Country_Zoom,
                    'center_lat'    => $country->country_centre_lat,
                    'center_lng'    => $country->country_centre_long

                );
            }
        }

        return $_country;
    }


    /**
     * Get all country data based on filter
     *
     * @param $filters
     * @return array
     * @throws Exception
     */
    public function getAll( $filters = array() ){

        $where = array();

        // can be array fo devices
        $device_id = isset( $filters['devices_id'] ) ? $filters['devices_id'] : 0;

        if( is_array( $device_id ) && count( $device_id ) > 0 ){
            $where[] = 'tdi.Device_Id IN ('. implode(',', $device_id ) .')';

        }
        elseif( $device_id > 0 ){
            $where[] = 'tdi.Device_Id = '. $device_id;

        }

        $where = implode(' AND ', $where );
        if( $where ){
            $where = "WHERE $where";
        }

        //$sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat, c.country_centre_long FROM tbl_country AS c INNER JOIN tbl_clinics AS tc ON tc.Country_Id = c.Country_Id INNER JOIN tbl_device_install AS tdi ON tdi.Clinic_Id = tc.Clinic_Id $where";
		
		$sql = "SELECT DISTINCT c.Country_Id, c.Country_Latitude, c.Country_Longitude, c.Country_Name, c.Country_Zoom, c.country_centre_lat, c.country_centre_long FROM tbl_country AS c $where";
		
        $query = $this->db->query( $sql );

        // echo $sql;die;

        return $query->result();



        /**
        if( isset($filters['devices_id']) && $filters['devices_id'] > 0 ){
            $this->db->join('tbl_map_report', 'tbl_map_report.Country_Id = tbl_country.Country_Id');

            if( is_array( $filters['devices_id']  ) && count( $filters['devices_id']  ) > 0 ){
                $this->db->where_in('tbl_map_report.Device_Id', $filters['devices_id'] );
            }
            elseif( $filters['devices_id']  > 0 ){
                $this->db->where('tbl_map_report.Device_Id', $filters['devices_id'] );
            }

        }


        $query = $this->db->get('tbl_country');

        return $query->result();
        */
    }

}