<?php

define("MODE","production");  // ["local", "production", "staging"]

switch( strtoupper(MODE) ){
    default:
    case "LOCAL":
        define("SITE_URL","http://103.58.145.110:9097");
        break;
    case "STAGING":
        define("SITE_URL","http://imdad.tiarait.com");
        break;
    case "PRODUCTION":
        define("SITE_URL","http://localhost/imdadplus");
        break;
}
