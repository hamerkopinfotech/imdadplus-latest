ALTER TABLE `tbl_map_report_marketing` ADD `City_Id` INT(6) NOT NULL AFTER `Country_Id`;
ALTER TABLE `tbl_map_report_marketing` ADD INDEX(`City_Id`);
ALTER TABLE `tbl_city` ADD `marker_type` ENUM('nothing','office','service_hub','both') NOT NULL AFTER `city_centre_long`;

UPDATE `tbl_country` SET `Country_Latitude` = '23.885942', `Country_Longitude` = '45.079162', `country_centre_lat` = '26.152254', `country_centre_long` = '45.813284' WHERE `tbl_country`.`Country_Id` = 1;
UPDATE `tbl_country` SET `Country_Latitude` = '24.250095', `Country_Longitude` = '54.170351', `country_centre_lat` = '24.250095', `country_centre_long` = '54.170351' WHERE `tbl_country`.`Country_Id` = 2;
UPDATE `tbl_country` SET `Country_Latitude` = '29.499976', `Country_Longitude` = '47.666493', `country_centre_lat` = '29.499976', `country_centre_long` = '47.666493' WHERE `tbl_country`.`Country_Id` = 3;
UPDATE `tbl_country` SET `Country_Latitude` = '25.535394', `Country_Longitude` = '51.655781', `country_centre_lat` = '25.396095022940987', `country_centre_long` = '51.1820672734375' WHERE `tbl_country`.`Country_Id` = 4;
UPDATE `tbl_country` SET `Country_Latitude` = '20.885758', `Country_Longitude` = '56.575469', `country_centre_lat` = '20.991375', `country_centre_long` = '56.18441' WHERE `tbl_country`.`Country_Id` = 5;
UPDATE `tbl_country` SET `Country_Latitude` = '26.118192', `Country_Longitude` = '50.533124', `country_centre_lat` = '26.145472545799883', `country_centre_long` = '50.54629690077968' WHERE `tbl_country`.`Country_Id` = 6;
UPDATE `tbl_country` SET `Country_Latitude` = '31.589357', `Country_Longitude` = '36.885350', `country_centre_lat` = '31.282527755156103', `country_centre_long` = '36.28675094955679' WHERE `tbl_country`.`Country_Id` = 7;
INSERT INTO `tbl_country` (`Country_Id`, `Country_Name`, `Country_Latitude`, `Country_Longitude`, `Country_Zoom`, `country_centre_lat`, `country_centre_long`) VALUES (NULL, 'Lebanon', '33.689210', '35.797435', '8', '33.689210', '35.797435');

UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 44;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 47;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 6;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 25;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 26;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 30;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 36;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 62;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 67;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 69;
UPDATE `tbl_city` SET `marker_type` = "both" WHERE `tbl_city`.`City_Id` = 77;

UPDATE `tbl_city` SET `marker_type` = 'both' WHERE `tbl_city`.`City_Id` = 22;
UPDATE `tbl_city` SET `marker_type` = 'both' WHERE `tbl_city`.`City_Id` = 25;
UPDATE `tbl_city` SET `marker_type` = 'both' WHERE `tbl_city`.`City_Id` = 74;

-- By Nishad 05/08/2018

-- Country Coordinates

UPDATE `tbl_country` SET `Country_Latitude` = '23.805942', `Country_Longitude` = '45.509162' WHERE `tbl_country`.`Country_Id` = 1;
UPDATE `tbl_country` SET `Country_Latitude` = '23.000095', `Country_Longitude` = '54.600351' WHERE `tbl_country`.`Country_Id` = 2;
UPDATE `tbl_country` SET `Country_Latitude` = '29.409976', `Country_Longitude` = '47.806493' WHERE `tbl_country`.`Country_Id` = 3;
UPDATE `tbl_country` SET `Country_Latitude` = '24.625394', `Country_Longitude` = '51.405781' WHERE `tbl_country`.`Country_Id` = 4;
UPDATE `tbl_country` SET `Country_Latitude` = '20.605758', `Country_Longitude` = '56.805469' WHERE `tbl_country`.`Country_Id` = 5;
UPDATE `tbl_country` SET `Country_Latitude` = '25.808192', `Country_Longitude` = '50.803124' WHERE `tbl_country`.`Country_Id` = 6;
UPDATE `tbl_country` SET `Country_Latitude` = '30.809357', `Country_Longitude` = '36.925350' WHERE `tbl_country`.`Country_Id` = 7;
UPDATE `tbl_country` SET `Country_Latitude` = '33.65978251', `Country_Longitude` = '36.10206026' WHERE `tbl_country`.`Country_Id` = 8;

UPDATE `tbl_country` SET `country_centre_lat` = '24.865173', `country_centre_long` = '55.174868' WHERE `tbl_country`.`Country_Id` = 2;

-- Country Coordinates

-- City Coordinates under Saudi

UPDATE `tbl_city` SET `City_Latitude` = '18.206468', `City_Longitude` = '42.511724' WHERE `tbl_city`.`City_Id` = 2; UPDATE `tbl_city` SET `City_Latitude` = '23.85522', `City_Longitude` = '42.912495' WHERE `tbl_city`.`City_Id` = 3; UPDATE `tbl_city` SET `City_Latitude` = '20.251741', `City_Longitude` = '41.501273' WHERE `tbl_city`.`City_Id` = 1; UPDATE `tbl_city` SET `City_Latitude` = '29.887356', `City_Longitude` = '39.320624' WHERE `tbl_city`.`City_Id` = 4; UPDATE `tbl_city` SET `City_Latitude` = '24.105537', `City_Longitude` = '47.311945' WHERE `tbl_city`.`City_Id` = 5; UPDATE `tbl_city` SET `City_Latitude` = '26.217191', `City_Longitude` = '50.197138' WHERE `tbl_city`.`City_Id` = 6; UPDATE `tbl_city` SET `City_Latitude` = '27.47117', `City_Longitude` = '48.497438' WHERE `tbl_city`.`City_Id` = 7; UPDATE `tbl_city` SET `City_Latitude` = '19.043429', `City_Longitude` = '42.167148' WHERE `tbl_city`.`City_Id` = 8; UPDATE `tbl_city` SET `City_Latitude` = '26.576492', `City_Longitude` = '49.998236' WHERE `tbl_city`.`City_Id` = 9; UPDATE `tbl_city` SET `City_Latitude` = '24.907056', `City_Longitude` = '45.900618' WHERE `tbl_city`.`City_Id` = 10; UPDATE `tbl_city` SET `City_Latitude` = '25.801747', `City_Longitude` = '43.522231' WHERE `tbl_city`.`City_Id` = 12; UPDATE `tbl_city` SET `City_Latitude` = '30.909945', `City_Longitude` = '41.309564' WHERE `tbl_city`.`City_Id` = 11; UPDATE `tbl_city` SET `City_Latitude` = '19.096906', `City_Longitude` = '42.863787' WHERE `tbl_city`.`City_Id` = 13; UPDATE `tbl_city` SET `City_Latitude` = '19.804887', `City_Longitude` = '41.651215' WHERE `tbl_city`.`City_Id` = 14; UPDATE `tbl_city` SET `City_Latitude` = '43.645402', `City_Longitude` = '-79.392094' WHERE `tbl_city`.`City_Id` = 15; UPDATE `tbl_city` SET `City_Latitude` = '26.422667', `City_Longitude` = '49.957714' WHERE `tbl_city`.`City_Id` = 16; UPDATE `tbl_city` SET `City_Latitude` = '24.4567', `City_Longitude` = '44.418179' WHERE `tbl_city`.`City_Id` = 17; UPDATE `tbl_city` SET `City_Latitude` = '26.236125', `City_Longitude` = '50.039302' WHERE `tbl_city`.`City_Id` = 18; UPDATE `tbl_city` SET `City_Latitude` = '28.329521', `City_Longitude` = '45.942012' WHERE `tbl_city`.`City_Id` = 19; UPDATE `tbl_city` SET `City_Latitude` = '27.45141', `City_Longitude` = '41.800824' WHERE `tbl_city`.`City_Id` = 20; UPDATE `tbl_city` SET `City_Latitude` = '25.314156', `City_Longitude` = '49.629908' WHERE `tbl_city`.`City_Id` = 21; UPDATE `tbl_city` SET `City_Latitude` = '21.789359', `City_Longitude` = '39.309359' WHERE `tbl_city`.`City_Id` = 22; UPDATE `tbl_city` SET `City_Latitude` = '16.859359', `City_Longitude` = '42.700567' WHERE `tbl_city`.`City_Id` = 23; UPDATE `tbl_city` SET `City_Latitude` = '26.909771', `City_Longitude` = '49.568742' WHERE `tbl_city`.`City_Id` = 24; UPDATE `tbl_city` SET `City_Latitude` = '18.409339', `City_Longitude` = '43.306233' WHERE `tbl_city`.`City_Id` = 25;

UPDATE `tbl_city` SET `City_Latitude` = '24.524654', `City_Longitude` = '39.709184' WHERE `tbl_city`.`City_Id` = 26; UPDATE `tbl_city` SET `City_Latitude` = '25.865231', `City_Longitude` = '45.415683' WHERE `tbl_city`.`City_Id` = 27; UPDATE `tbl_city` SET `City_Latitude` = '21.389082', `City_Longitude` = '39.907912' WHERE `tbl_city`.`City_Id` = 28; UPDATE `tbl_city` SET `City_Latitude` = '17.505604', `City_Longitude` = '44.228944' WHERE `tbl_city`.`City_Id` = 29; UPDATE `tbl_city` SET `City_Latitude` = '25.827489', `City_Longitude` = '42.863787' WHERE `tbl_city`.`City_Id` = 30; UPDATE `tbl_city` SET `City_Latitude` = '31.409635', `City_Longitude` = '38.001353' WHERE `tbl_city`.`City_Id` = 32; UPDATE `tbl_city` SET `City_Latitude` = '29.508789', `City_Longitude` = '43.702611' WHERE `tbl_city`.`City_Id` = 33; UPDATE `tbl_city` SET `City_Latitude` = '21.202738', `City_Longitude` = '42.85404' WHERE `tbl_city`.`City_Id` = 34; UPDATE `tbl_city` SET `City_Latitude` = '26.753008', `City_Longitude` = '49.958236' WHERE `tbl_city`.`City_Id` = 35; UPDATE `tbl_city` SET `City_Latitude` = '24.713552', `City_Longitude` = '46.675296' WHERE `tbl_city`.`City_Id` = 36; UPDATE `tbl_city` SET `City_Latitude` = '17.154798', `City_Longitude` = '42.626897' WHERE `tbl_city`.`City_Id` = 37; UPDATE `tbl_city` SET `City_Latitude` = '30.108003', `City_Longitude` = '40.304306' WHERE `tbl_city`.`City_Id` = 38; UPDATE `tbl_city` SET `City_Latitude` = '25.157647', `City_Longitude` = '45.252479' WHERE `tbl_city`.`City_Id` = 39; UPDATE `tbl_city` SET `City_Latitude` = '28.703508', `City_Longitude` = '36.706191' WHERE `tbl_city`.`City_Id` = 40; UPDATE `tbl_city` SET `City_Latitude` = '21.557273', `City_Longitude` = '40.602714' WHERE `tbl_city`.`City_Id` = 41; UPDATE `tbl_city` SET `City_Latitude` = '20.127193', `City_Longitude` = '44.900549' WHERE `tbl_city`.`City_Id` = 42; UPDATE `tbl_city` SET `City_Latitude` = '24.303176', `City_Longitude` = '38.309978' WHERE `tbl_city`.`City_Id` = 43;

-- City Coordinates under Saudi

-- City Coordinates under UAE

UPDATE `tbl_city` SET `City_Latitude` = '24.4096', `City_Longitude` = '54.4561' WHERE `tbl_city`.`City_Id` = 44; UPDATE `tbl_city` SET `City_Latitude` = '25.38234', `City_Longitude` = '55.557937' WHERE `tbl_city`.`City_Id` = 45; UPDATE `tbl_city` SET `City_Latitude` = '24.103233', `City_Longitude` = '55.7634085' WHERE `tbl_city`.`City_Id` = 46; UPDATE `tbl_city` SET `City_Latitude` = '25.15442', `City_Longitude` = '55.3097' WHERE `tbl_city`.`City_Id` = 47; UPDATE `tbl_city` SET `City_Latitude` = '25.411076', `City_Longitude` = '56.248228' WHERE `tbl_city`.`City_Id` = 48; UPDATE `tbl_city` SET `City_Latitude` = '25.754185', `City_Longitude` = '56.004015' WHERE `tbl_city`.`City_Id` = 49; UPDATE `tbl_city` SET `City_Latitude` = '25.300845', `City_Longitude` = '55.4553' WHERE `tbl_city`.`City_Id` = 50; UPDATE `tbl_city` SET `City_Latitude` = '25.450455', `City_Longitude` = '55.57829' WHERE `tbl_city`.`City_Id` = 51;

-- City Coordinates under UAE

-- City Coordinates under Oman

UPDATE `tbl_city` SET `City_Latitude` = '28.574513', `City_Longitude` = '48.102474' WHERE `tbl_city`.`City_Id` = 54; UPDATE `tbl_city` SET `City_Latitude` = '29.346573', `City_Longitude` = '47.675529' WHERE `tbl_city`.`City_Id` = 52; UPDATE `tbl_city` SET `City_Latitude` = '29.349965', `City_Longitude` = '48.028283' WHERE `tbl_city`.`City_Id` = 53; UPDATE `tbl_city` SET `City_Latitude` = '29.301989', `City_Longitude` = '48.054087' WHERE `tbl_city`.`City_Id` = 55; UPDATE `tbl_city` SET `City_Latitude` = '29.377804', `City_Longitude` = '47.99979' WHERE `tbl_city`.`City_Id` = 56; UPDATE `tbl_city` SET `City_Latitude` = '29.083128', `City_Longitude` = '48.133464' WHERE `tbl_city`.`City_Id` = 57; UPDATE `tbl_city` SET `City_Latitude` = '29.281596', `City_Longitude` = '47.960252' WHERE `tbl_city`.`City_Id` = 58; UPDATE `tbl_city` SET `City_Latitude` = '29.169667', `City_Longitude` = '48.117577' WHERE `tbl_city`.`City_Id` = 59; UPDATE `tbl_city` SET `City_Latitude` = '29.335298', `City_Longitude` = '48.009893' WHERE `tbl_city`.`City_Id` = 60; UPDATE `tbl_city` SET `City_Latitude` = '29.318057', `City_Longitude` = '48.025805' WHERE `tbl_city`.`City_Id` = 61; UPDATE `tbl_city` SET `City_Latitude` = '29.31166', `City_Longitude` = '47.481766' WHERE `tbl_city`.`City_Id` = 62; UPDATE `tbl_city` SET `City_Latitude` = '29.149656', `City_Longitude` = '48.119199' WHERE `tbl_city`.`City_Id` = 63; UPDATE `tbl_city` SET `City_Latitude` = '29.361264', `City_Longitude` = '47.983479' WHERE `tbl_city`.`City_Id` = 64; UPDATE `tbl_city` SET `City_Latitude` = '29.322294', `City_Longitude` = '48.081561' WHERE `tbl_city`.`City_Id` = 65; UPDATE `tbl_city` SET `City_Latitude` = '29.313775', `City_Longitude` = '48.00846' WHERE `tbl_city`.`City_Id` = 66;

-- City Coordinates under Oman

-- City Coordinates under Qatar

UPDATE `tbl_city` SET `City_Latitude` = '25.273864', `City_Longitude` = '51.505159' WHERE `tbl_city`.`City_Id` = 67;

-- City Coordinates under Qatar

-- City Coordinates under Oman

UPDATE `tbl_city` SET `City_Latitude` = '24.167141', `City_Longitude` = '56.204225' WHERE `tbl_city`.`City_Id` = 68; UPDATE `tbl_city` SET `City_Latitude` = '23.34402303', `City_Longitude` = '58.4278953' WHERE `tbl_city`.`City_Id` = 69; UPDATE `tbl_city` SET `City_Latitude` = '17.1540438', `City_Longitude` = '53.90896' WHERE `tbl_city`.`City_Id` = 70; UPDATE `tbl_city` SET `City_Latitude` = '24.1457512', `City_Longitude` = '56.7624457' WHERE `tbl_city`.`City_Id` = 71;

-- City Coordinates under Oman

-- City Coordinates under Bahrain

UPDATE `tbl_city` SET `City_Latitude` = '26.214488', `City_Longitude` = '50.584754' WHERE `tbl_city`.`City_Id` = 72; UPDATE `tbl_city` SET `City_Latitude` = '26.228265', `City_Longitude` = '50.495392' WHERE `tbl_city`.`City_Id` = 73; UPDATE `tbl_city` SET `City_Latitude` = '26.231048', `City_Longitude` = '50.586164' WHERE `tbl_city`.`City_Id` = 74; UPDATE `tbl_city` SET `City_Latitude` = '26.269898', `City_Longitude` = '50.622255' WHERE `tbl_city`.`City_Id` = 75; UPDATE `tbl_city` SET `City_Latitude` = '26.125606', `City_Longitude` = '50.534262' WHERE `tbl_city`.`City_Id` = 76;

-- City Coordinates under Bahrain

-- City Coordinates under Jordan


UPDATE `tbl_city` SET `City_Latitude` = '31.85678436', `City_Longitude` = '35.9538647' WHERE `tbl_city`.`City_Id` = 77; UPDATE `tbl_city` SET `City_Latitude` = '32.475814', `City_Longitude` = '35.899882' WHERE `tbl_city`.`City_Id` = 78;

-- City Coordinates under Jordan

-- By Nishad 05/08/2018
