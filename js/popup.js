function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    _val = n.toString();
    _found = _val.indexOf(".");
    if( _found > -1 ){
        return true;
    }
    return false;
    return Number(n) === n && n % 1 !== 0;
}

FloatingPopup = {

    __containers: [        
            'clients'                     ,
            'solutions'                   ,
            'market_share'                ,
            'avg_repair_time_12h_warranty',
            'total_repair_calls_warranty' ,
            'avg_repair_time_12h_all'     ,
            'total_repair_calls_all'      ,
            'first_time_fix_rate'         ,
            'ftfr_total_repair_calls_all' ,
            'up_time'                     ,
            'doctors_trained'             ,
            'nurses_trained'              ,
            'e_learning_course_delivered' ,
            'patient_leads'               ,
            'device_name'                 ,
            'device_id'                   ,             
            'microsite_visitors'          ,             
            'locator_actions'             ,             
            'qualified_leads'             ,
        ],
    // loading report to the floating poup sidebar
    populate_popup: function( creport ){
        //console.log ( "Report v2: " , creport );
        jQuery.each(this.__containers,function(i,item){
            console.log(item);
            console.log( "isFloat for " + item + " : " , isFloat( eval("creport."+item) ));
            var __value = ( isFloat( eval( "creport."+item ) ) ) ? parseFloat( eval( "creport."+item ) ).format(2) : parseInt( eval( "creport."+item ) ).format();
            jQuery("#"+item + " span").html( __value );
        });
    },

    animate_numbers: function(){
    }
}

//FloatingPopup.populate_popup();






