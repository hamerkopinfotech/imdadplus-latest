var gmarkers1 = [];
var tempmarkers = [];
var markers1 = [];
var pointers = [];
var breadcrumb = [];
var infowindow = new google.maps.InfoWindow({content: ''});
var bounds = new google.maps.LatLngBounds();
var filters = [];
var markerOpacity = markerOpacityIncrement = 0.05;

var iconBase = '/imdadplus/img/markers/';
var icons = {
    "country": {
        icon: iconBase + 'marker-blue.png'
    },
    city: {
        icon: iconBase + 'marker-blue.png'
    }
};

var json = JSON.parse($.getJSON({'url': "/imdadplus/api/report/", 'async': false}).responseText);
//var json = JSON.parse($.getJSON({'url': "data.php", 'async': false}).responseText);
//var json = JSON.parse($.getJSON({'url': "/imdadplus/data.json", 'async': false}).responseText);

/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/**
 * Function to init map
 */

function initialize() {
    var mecoord = new google.maps.LatLng(23.8859, 45.0792);
    var center = new google.maps.LatLng(31.7917, 7.0926);
    var mapOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        scrollwheel: false,
        disableDoubleClickZoom: true, // <---
        panControl: false,
        streetViewControl: false,
        // disableDefaultUI: true
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    map.setCenter(mecoord);


    setTimeout(function () {
        map.panTo(mecoord);
        load_markers('gcc', 'gcc');
    }, 2000);

    breadcrumb.push("gcc");
    //fit_bounds_markers(5);
    // map.setZoom(6);
    // fit_bounds_markers(7);
    //goback();
}

goback();

function goback() {
    jQuery("#goback").on("click", function () {
        console.log(breadcrumb);
        console.log('breadcrumb.length'+breadcrumb.length);
        $('#myInput').val('');
        switch (breadcrumb.length) {
            case 4:
                delete_markers();
                countryslugback = breadcrumb[1]
                cityslugback = breadcrumb[2]
                console.log("Country, ", countryslugback);
                console.log("Zoom : ", "json.countries." + countryslugback + ".cities." + cityslugback + ".zoom");
                var zoom = eval("json.countries." + countryslugback + ".cities." + cityslugback + ".zoom");

                // if( zoom == "undefined" || zoom == ""){
                //     zoom = 9;
                // }
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }
                //map.setZoom(8)

                console.log("Back Country Zoom", "From : " + map.getZoom() + " To : " + zoom);

                load_previous_markers("city", cityslugback);

                // if( countryslugback == "uae" ){
                //     set_center("United Arab Emirates"); // ksa center;
                // }else if( countryslugback == "ksa" ){
                //     set_center("Kingdom of Saudi Arabia"); // ksa center;
                // }else{
                //     set_center(eval("json.countries."+countryslugback+".label"));
                // }

                set_center(eval("json.countries." + countryslugback + ".cities." + cityslugback + ".center_lat") + "," + eval("json.countries." + countryslugback + ".cities." + cityslugback + ".center_lng"));

                // now populate the markers in the zoomed map
                // setTimeout(function(){
                //     render_markers();
                // },500);

                // fit_bounds_markers(zoom);
                breadcrumb.pop();
                break;
            case 3:
                delete_markers();
                countryslugback = breadcrumb[1]
                console.log("Country, ", countryslugback);

                var zoom = eval("json.markers." + countryslugback + ".zoom");

                // if( zoom == "undefined" || zoom == ""){
                //     zoom = 9;
                // }
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }
                //map.setZoom(8)

                console.log("Back Country Zoom", "From : " + map.getZoom() + " To : " + zoom);

                load_previous_markers("country", countryslugback);

                // if( countryslugback == "uae" ){
                //     set_center("United Arab Emirates"); // ksa center;
                // }else if( countryslugback == "ksa" ){
                //     set_center("Kingdom of Saudi Arabia"); // ksa center;
                // }else{
                //     set_center(eval("json.countries."+countryslugback+".label"));
                // }

                set_center(eval("json.countries." + countryslugback + ".center_lat") + "," + eval("json.countries." + countryslugback + ".center_lng"));

                // now populate the markers in the zoomed map
                // setTimeout(function(){
                //     render_markers();
                // },500);

                // fit_bounds_markers(zoom);
                breadcrumb.pop();
                break;
            case 2:
                delete_markers();
                console.log("I am in region view");
                //load_previous_markers("gcc","gcc");
                //smoothZoomV2(map, 6, map.getZoom(), false);
                initialize();
                //console.log(json.report);

                // center map
                var mecoord = new google.maps.LatLng(23.8859, 45.0792);
                map.setCenter(mecoord);

                jQuery(".mapview-label").html("Middle East");
                //FloatingPopup.populate_popup(json.report.alldevice);
                // jQuery("#goback").hide();
                breadcrumb = ["gcc"];
                // console.log("Country, " , breadcrumb[0]);
                // load_previous_markers("gcc","gcc");
                // var zoom = 9;
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }

                // map.setZoom(zoom)

                // breadcrumb.pop();
                break;
            default:
                //FloatingPopup.populate_popup(json.report);
                console.log("Nothign to view from this point backward.");
        }
        console.log("Breadcrumbs", breadcrumb);
        check_val();
    });
}

function check_val() {
    var val = $('#mapview').find('span.mapview-label').text();
    console.log(val);
    if (val === 'Middle East') {
        $('#goback').hide();
    } else {
        $('#goback').show();
    }
}

check_val();
initialize();

// FloatingPopup.populate_popup(json.report.alldevice);
setTimeout(function () {
    FloatingPopup.populate_popup(json.report.alldevice);
}, 1000);


jQuery(function ($) {

    var link = $("ul.imdad-tab li a");
    link.on('click', function (e) {
        e.preventDefault();
        var a = $(this).attr("href");
        $(a).slideToggle('fast');
        $(".imdad-tab-content .tab-pane").not(a).slideUp('fast');
    });

//    if ($('input[type=checkbox]').is(':checked')) {
//        $('#reset').show();
//    } else {
//        $('#reset').hide();
//    }

    $('.form-check-input').on('click', function (e) {
        filters = [];
        filter_counter = 0;
        jQuery("input[type='checkbox']:checked").each(function (i) {
            filters.push("device[]=" + jQuery(this).val());
            filter_counter++;
        });
        if (filter_counter > 0) {
            jQuery.get("/imdadplus/api/report/?" + filters.join("&"), function (response) {
                json = response;
                reload_map();
            });
        }
        if ($("input[type='checkbox']:checked").length > 0) {
//            $('#reset').show();
        } else {
            filters = [];
            json = JSON.parse($.getJSON({'url': "/imdadplus/api/report/", 'async': false}).responseText);
            reload_map();
//            $('#reset').hide();
        }
    });

});


    function slugify(string) {
      return string
        .toString()
        .trim()
        .toLowerCase()
        .replace(/\s+/g, "-")
        .replace(/[^\w\-]+/g, "")
        .replace(/\-\-+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
    }
  $( function() {
      
    $( "#myInput" ).autocomplete({
      maxResults: 5,
      source: function(request, response) {   
        filters = [];
        filter_counter = 0;
        jQuery("input[type='checkbox']:checked").each(function (i) {
            filters.push("device[]=" + jQuery(this).val());
            filter_counter++;
        });
        if (filter_counter > 0) {
            jQuery.get("/imdadplus/api/report/?" + filters.join("&"), function (responseapi) {
                json = responseapi;
                var merged = [];
                $.each(json.clinics, function( index, value ) {
                    $.each(value, function( indinner, valinner ) {
                        $.each(valinner, function( inddeep, valdeep ) {
                            merged.push(valdeep.label);
                        });
                    });
                });               
                var label = $('.mapview-label').html();
                var labelsplit = label.split(",");

                var len = labelsplit.length;
                var availableTags;
                if(len == 1 && labelsplit[0] == 'Middle East') { // region view
                    availableTags = JSON.parse($('#clinicdata').val());
                } else if(len == 1 && labelsplit[0] != 'Middle East' ) { // country view
                     labelsplit[0] = slugify(labelsplit[0]);
                     available = JSON.parse($('#clinicdatacountry').val());
                     availableTags = available[labelsplit[0].toLowerCase().trim()];
                } else if(len == 2 ) { // city view
                     labelsplit[0] = slugify(labelsplit[0]);
                     labelsplit[1] = slugify(labelsplit[1]);
                     available = JSON.parse($('#clinicdatacountry').val());
                     availableTags = available[labelsplit[1].toLowerCase().trim()];
//                     available = JSON.parse($('#clinicdatacity').val()); 
//                     availableTags = available[labelsplit[1].toLowerCase().trim()][labelsplit[0].toLowerCase().trim()];
                } else if(len == 3 ) { // clinic view
                     labelsplit[0] = slugify(labelsplit[0]);
                     labelsplit[1] = slugify(labelsplit[1]);
                     labelsplit[2] = slugify(labelsplit[2]);
                     available = JSON.parse($('#clinicdatacountry').val());
                     availableTags = available[labelsplit[2].toLowerCase().trim()];
//                     available = JSON.parse($('#clinicdatacity').val()); 
//                     availableTags = available[labelsplit[2].toLowerCase().trim().replace('<br>','')][labelsplit[1].toLowerCase().trim().replace('<br>','')];
                }

                var common = $.grep(merged, function(element) {
                    return $.inArray(element, availableTags ) !== -1;
                });
                common = $.unique(common);
                var results = $.ui.autocomplete.filter(common, request.term);

                response(results.slice(0, 5));
            });
        } else {
          var label = $('.mapview-label').html();
          var labelsplit = label.split(",");
          var len = labelsplit.length;
          var availableTags;
          if(len == 1 && labelsplit[0] == 'Middle East') { // region view
              availableTags = JSON.parse($('#clinicdata').val());
          } else if(len == 1 && labelsplit[0] != 'Middle East' ) { // country view
               labelsplit[0] = slugify(labelsplit[0]);
               available = JSON.parse($('#clinicdatacountry').val());
               availableTags = available[labelsplit[0].toLowerCase().trim()];
          } else if(len == 2 ) { // city view
            labelsplit[0] = slugify(labelsplit[0]);
            labelsplit[1] = slugify(labelsplit[1]);
            available = JSON.parse($('#clinicdatacountry').val());
            availableTags = available[labelsplit[1].toLowerCase().trim()];
//            available = JSON.parse($('#clinicdatacity').val()); 
//            availableTags = available[labelsplit[1].toLowerCase().trim()][labelsplit[0].toLowerCase().trim()];
          } else if(len == 3 ) { // clinic view
            labelsplit[0] = slugify(labelsplit[0]);
            labelsplit[1] = slugify(labelsplit[1]);
            labelsplit[2] = slugify(labelsplit[2]);
            available = JSON.parse($('#clinicdatacountry').val());
            availableTags = available[labelsplit[2].toLowerCase().trim()];
//            available = JSON.parse($('#clinicdatacity').val()); 
//            availableTags = available[labelsplit[2].toLowerCase().trim().replace('<br>','')][labelsplit[1].toLowerCase().trim().replace('<br>','')];
          }
            var results = $.ui.autocomplete.filter(availableTags, request.term);

            response(results.slice(0, this.options.maxResults));
        }
      }
      ,
        select: function( event, ui ) {
            $('#market_share').hide().removeClass('col-lg-4').addClass('col-lg-6');
            $('#equipment').removeClass('col-lg-4').addClass('col-lg-6');
            $('#clients').removeClass('col-lg-4').addClass('col-lg-6');
            $('.orange').hide();
            $('.red').hide();
            $('.light-blue').hide();
            var country = document.getElementById('country_'+ui.item.value).value;
            var city = document.getElementById('city_'+ui.item.value).value;
            var clinic = document.getElementById('clinic_'+ui.item.value).value;
            load_markers_indirect(country,city,clinic);
        }
    });
  } );
  

