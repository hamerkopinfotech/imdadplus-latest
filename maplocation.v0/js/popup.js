FloatingPopup = {

    __containers: [        
            'clients'                     ,
            'solutions'                   ,
            'market_share'                ,
            'avg_repair_time_12h_warranty',
            'total_repair_calls_warranty' ,
            'avg_repair_time_12h_all'     ,
            'total_repair_calls_all'      ,
            'first_time_fix_rate'         ,
            'ftfr_total_repair_calls_all' ,
            'up_time'                     ,
            'doctors_trained'             ,
            'nurses_trained'              ,
            'e_learning_course_delivered' ,
            'patient_leads'               ,
            'device_name'                 ,
            'device_id'                   
        ],
    // loading report to the floating poup sidebar
    populate_popup: function( report ){
        console.log ( "Report : " , report );
        jQuery.each(this.__containers,function(i,item){
            console.log(item);
            jQuery("#"+item + " span").html(parseInt(eval("report."+item)).format());
        });
    },

    animate_numbers: function(){
    }
}

//FloatingPopup.populate_popup();






